package convert;

import convert.ReadConfig;

/**
 * 
 * This refers the serial input to the configuration method
 * @author Tyler J. Sturos
 *
 */
public class Lookup {
	
	public static String[] lookup(String serialIn) {
		
		
		return ReadConfig.getConfig(serialIn);
		

	}

}