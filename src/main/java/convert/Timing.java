package convert;

public class Timing {
	
	private long startTime;
	private long endTime;
	
	public Timing() {
		setStart();
	}

	private void setStart() {
		startTime = System.currentTimeMillis();
		System.out.println("Started a reading.");
	}
	
	private  void setEndTime() {
		endTime = System.currentTimeMillis();
	}
	
	
	/**
	 * Returns a statement using the calculated time elapsed (ms) since this timing has been initiated.
	 * @return String describing execution time
	 */
	public String getTimeDifference() {
		setEndTime();
		//elapsedTime = (int) (endTime - startTime);
		return "Total execution time: " + (endTime - startTime) + "ms";
	}
	
	
}
