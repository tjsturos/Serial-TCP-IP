package convert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JOptionPane;

public class ReadConfig {

	// 
	public static String[] getConfig(String serialIn) {

		// get the program folder location and config file
		String root = SystemUtilities.getRoot();
		String csvFile = root.concat(File.separator).concat("SerialConverter/config.cfg");

		// check to see if the file has been setup
		File f = new File(csvFile);
		if(!f.exists()) {

			// tell user of the problem (no config file)
			JOptionPane.showMessageDialog(null, "You are missing the config file, \"config.cfg\".\n\n"
					+ "The software will run, but will not send any messages to the printer until the file is created by running the config.bat file.");
			return null;
		}

		// header and row definitions
		// storage variables for sending data back
		String seperator = "X";

		String headRow = "+++";
		String head = "";
		
		String orderRow = "ORDER";
		String orderNumber = "";
		
		String dirRow = "FILEPATH-TO-SAVE";
		String dir = "";

		String symbolHeader = "!@@@";
		String symbolRow = "@@@@";
		boolean readSymbols = true;
		int[] symbolIndexes = new int[2];
		String symbol = "";

		String widthHeader = "!###";
		String widthRow = "####";
		boolean readWidths = true;
		int[] widthIndexes = new int[2];
		String width = "";

		String lengthHeader = "!---";
		String lengthRow = "----";
		boolean readLengths = true;
		int[] lengthIndexes = new int[2];
		String length = "";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			int rawLength = -1;
			int rawWidth = -1;
			int rawSymbol = -1;
			String line = "";
			System.out.println("=============== Starting Lookup Process ==============");
			
			while ((line = br.readLine()) != null) {
				
				
				if (line.startsWith(orderRow)) {
					System.out.println("-----------Looking for Order----------------");
					orderNumber = line.split("=")[1];
					System.out.println("The order is: " + orderNumber);
				} else if (line.startsWith(headRow)) {
					System.out.println("-----------Looking for Printer Head----------------");

					head = line.split("=")[1];
					System.out.println("The printer head is: " + head);
				} else if (line.startsWith(dirRow)) {
					System.out.println("-----------Looking for Save Path-----------------");
					
					// assign the right-hand value to the dir variable
					dir = line.split("=")[1];
					System.out.println("The save directory is: " + dir);
				} else if(line.startsWith(symbolHeader)) {
				
					System.out.println("-----------Starting Symbol Lookup----------------");

					// get symbol indexes
					symbolIndexes = getIndexes(line);
					System.out.println("The symbol indexes are: " + Arrays.toString(symbolIndexes));

					// get the symbol value from the input and then convert to int
					rawSymbol = getDimension(serialIn, symbolIndexes);
					System.out.println("The symbol input is: " + rawSymbol);
				} else if (line.startsWith(symbolRow) && readSymbols) {

					// get rid of the prefix
					String values = line.replaceAll("@", "");

					// split the symbol end value and the values associated with it
					String[] valuePair = values.split("=");

					// get all the values for this symbol into an array
					String[] valuesForSymbol = valuePair[1].split(",");

					for (String val : valuesForSymbol) {
						// check to see if the symbol value is equal this array element
						if( rawSymbol == Integer.parseInt(val)) {
							
							// if it is, set the symbol to this end value
							symbol = valuePair[0];
							System.out.println("The symbol output is: " + symbol);
							// stop reading symbols, as we know which one we have now
							readSymbols = false;
						}
					}


				} else if (line.startsWith(widthHeader)) {
					System.out.println("-----------Starting Width Lookup----------------");

					// getting width indexes
					widthIndexes = getIndexes(line);
					System.out.println("Width indexes are: " + Arrays.toString(widthIndexes));

					// extract the width from the serial code and convert to int
					rawWidth = getDimension(serialIn, widthIndexes);
					System.out.println("The width input is: " + rawWidth );
					
				} else if (line.startsWith(widthRow) && readWidths) {

					String widthToCheck = checkValue("#", line, rawWidth);
					// check to see if the raw width will fall into this line's range
					if (widthToCheck.length() > 0) {
						// set readWidths to false, so we don't waste any more processing power
						readWidths = false;
						width = widthToCheck;
						
						System.out.println("The width output is: " + width);
					}

				} else if (line.startsWith(lengthHeader)) {
					System.out.println("-----------Starting Length Lookup----------------");

					// get the length indexes 
					lengthIndexes = getIndexes(line);
					System.out.println("The length indexes are: " + Arrays.toString(lengthIndexes));
					
					// get the length from the serial code and convert it into an int
					rawLength = getDimension(serialIn, lengthIndexes);
					System.out.println("The length input is: " + rawLength);

				} else if (line.startsWith(lengthRow) && readLengths) {


					// check to see if it length falls into this line's range
					String lengthToCheck = checkValue("-", line, rawLength);

					if (lengthToCheck.length() > 0) {
						// set readLengths to false, so we don't waste any more processing power
						readLengths = false;
						length = lengthToCheck;
						System.out.println("The length output is: " + length);
					}

				}

			} // end of file reading while loop
			System.out.println("============== End of Lookup Process ==============");
			
			// return the head, as well as the file name
			String[] returnValues = new String[3];
			
			String[] dimensions = getOrder(orderNumber, new String[] {symbol, length, width});
			returnValues[0] = head;
			returnValues[1] = dimensions[0].concat(seperator).concat(dimensions[1]).concat(seperator).concat(dimensions[2]);
			returnValues[2] = dir;

			return returnValues;



		} catch (IOException e) {
			e.printStackTrace();
			return null;

		} // end of try/catch for reading the file
		
	} // end of getConfig method


	

	/**
	 * This will return either an empty string if the valueToCompare doesn't fall into the range contained
	 * on this line, or it will return a String containing a number representing that range.
	 * 
	 * Standard layout is as follows: [prefix, e.g. "****"][rangeOutput]=[rangeMin, rangeMax].  Full example: ****167=160,173
	 * 
	 * @param prefix String - the beginning of the line that marks it as a particular range
	 * @param line String - the String in which is contained a range and an output for that range
	 * @param valueToCompare int - the number in which needs to fit in the range, in order to get a correct output.
	 * @return
	 */
	private static String checkValue(String prefix, String line, int valueToCompare) {
		// get rid of all the prefixes
		String data = line.replaceAll(prefix, "");

		// get the ranges
		String[] dataPair = data.split("=");
		String[] widthRangeString = dataPair[1].split(",");
		int[] widthRange = {Integer.parseInt(widthRangeString[0]), Integer.parseInt(widthRangeString[1]) };

		// check to see if the raw width will fall into a range
		if (valueToCompare >= widthRange[0] && valueToCompare <= widthRange[1]) {

			return dataPair[0];

		} else return "";

	}

	/**
	 * This will return the indexes that are contained in the header rows (marked by a !). 
	 * 
	 * Standard layout is that the indexes are after the '=' character.  The indexes are then separated (min, max) by a comma, as well.
	 * 
	 * Full example: !***WIDTH=0,3
	 * 
	 * The prefixes and human readable title can be ignored as we don't need any data from the left side of the equals sign.
	 * 
	 * @param line String - it contains the indexes.
	 * @return
	 */
	private static int[] getIndexes(String line) {

		int[] returnValues = new int[2];

		// get the data separated, split on the '='
		String[] conversion = line.split("=");

		// then get the indexes by separating min and max
		String[] rawIndexes = conversion[1].split(",");
		returnValues[0] = Integer.parseInt(rawIndexes[0]);
		returnValues[1] = Integer.parseInt(rawIndexes[1]);
		
		// parse and return
		return returnValues;
	}

	/**
	 * This gets the dimensions from a particular location in the input string, by using the indexes given.
	 * 
	 * @param input String - the whole raw input.
	 * @param substring int[] - the location (startIndex, endIndex) of the dimension.
	 * @return
	 */
	private static int getDimension(String input, int[] substring) {
		try {
			// try to get the dimensions from the input, if it is the last dimension (meaning that it
			// will throw an IndexOutofBoundsException due to the "plus one") then catch the error and just
			// return the end of the input starting at the index given
			return Integer.parseInt(input.substring(substring[0], substring[1] + 1));
		} catch (IndexOutOfBoundsException e) {
			// just go to the end of the string
			return Integer.parseInt(input.substring(substring[0]));
		}
		
	}
	
	private static String[] getOrder(String orderNumber, String[] values) {
		
		String shape = values[0];
		String length = values[1];
		String width = values[2];
		
		String[] valuesInOrder;
		
		
		switch (Integer.valueOf(orderNumber)) {
			case 1:
				valuesInOrder = new String[]{shape, length, width};
				break;
			case 2:
				valuesInOrder = new String[]{shape, width, length};
				break;
			case 3:
				valuesInOrder = new String[]{length, width, shape};
				break;
			case 4:
				valuesInOrder = new String[]{length, shape, width};
				break;
			case 5:
				valuesInOrder = new String[]{width, shape, length};
				break;
			case 6:
				valuesInOrder = new String[]{width, length, shape};
				break;
			default:
				valuesInOrder = values;
			
		}
		
		return valuesInOrder;
	}



}
	


