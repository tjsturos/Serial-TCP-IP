package convert;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import convert.Lookup;
import convert.CreateFile;
import convert.TransmissionOut;

import jssc.SerialPort; 
import jssc.SerialPortEvent; 
import jssc.SerialPortEventListener; 
import jssc.SerialPortException;

/*
 * Reads a serial input and then sends a response back to the origin.
 * 
 */
public class SerialPortReader implements SerialPortEventListener {
	
	SerialPort serialPort;
	
	
	public SerialPortReader(SerialPort serialPort) { this.serialPort = serialPort; }

	@Override
    public void serialEvent(SerialPortEvent event) {
    	Timing stopwatch = new Timing();
    	
        if(event.isRXCHAR()){//If data is available
        	System.out.println("Reciving a character...");
            if(event.getEventValue() > 0){//Check bytes count in the input buffer
            	System.out.println("Event Value is: " + event.getEventValue());
                //Read data, if 10 bytes available 
                try {
                	
                	// get the bytes from the serial port
                	 byte buffer[] = serialPort.readBytes();
                	 String toProcess = new String(buffer, "UTF-8").trim();
                	 
                	 // log the message out
                	 System.out.println("Message is: " + new String(buffer, "UTF-8"));
                	 
                         new Thread(new Runnable() {
                        	    @Override public void run() {
                        	    	
                        	      // look up the file name from the input
                        	      String[] fileDetails = Lookup.lookup(toProcess);
                        	      System.out.println("Looked up " + toProcess + " and the result is: " + Arrays.toString(fileDetails));
                        	      
                        	      // if there are no errors looking up the filename, then create the file
                        	      if (fileDetails[0].length() > 0) {
                        	    	 CreateFile.create(fileDetails, stopwatch); 
                        	      } else {
                        	    	  // stop timing and print it out
                        	    	  System.out.println(stopwatch.getTimeDifference());
                        	      }
                        	      
                        	      //TransmissionOut.sendTcpOut(hex, stopwatch);
                        	    }
                        	}).start();
                     
                            
                         
                                   
                }
                catch (SerialPortException | UnsupportedEncodingException ex) {
                    System.out.println(ex);
                }
            }
        }
        else if(event.isCTS()){//If Clear To Send line has changed state
            if(event.getEventValue() == 1){//If line is ON
                System.out.println("CTS - ON");
            }
            else {
                System.out.println("CTS - OFF");
            }
        }
        else if(event.isDSR()){///If Data Set Ready line has changed state
            if(event.getEventValue() == 1){//If line is ON
                System.out.println("DSR - ON");
            }
            else {
                System.out.println("DSR - OFF");
            }
        }
        
	}
        
}