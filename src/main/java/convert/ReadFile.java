package convert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import javax.swing.JOptionPane;

import convert.SystemUtilities;

public class ReadFile {
	

	public static String[] getComPortSettings() {
		
		// get the system specifics and the file location
		String root = SystemUtilities.getRoot();
		String configFile = root.concat(File.separator).concat("SerialConverter/config.cfg");
		
		File f = new File(configFile);
		if(!f.exists()) { 
			JOptionPane.showMessageDialog(null, "You are missing the configuration file (Edit " + configFile + ").\n\n"
					+ "The software will not run until the file is created.");
			return new String[]{"ERROR: Lookup table not found.", ""};
		}
		
		// determine how the file is structured
        String line = "";
        String comPortHeader = "...";

        try (BufferedReader br = new BufferedReader(new FileReader(configFile))) {

            while ((line = br.readLine()) != null) {
            	
            	// skip the header row
            	if (line.startsWith(comPortHeader)) {
            		return line.split("=")[1].split(",");
            	};
            	
                
                

            }
            
            return new String[] {"ERROR: No Config row found.", ""};
            

        } catch (IOException e) {
            e.printStackTrace();
        	return new String[]{"ERROR: Error parsing file.", ""};

        }
        

	}

}
