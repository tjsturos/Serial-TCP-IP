package convert;

import convert.SerialPortReader;
import convert.ReadFile;
import jssc.SerialPort; 
import jssc.SerialPortException;

public class ConvertCode {

//	public static void main(String[] args) {
//		String libPathProperty = System.getProperty("java.library.path");
//        System.out.println(libPathProperty);
//		new ReadSerial();
//		
//	}
	static SerialPort serialPort;
	
	public static void main(String[] args) throws Exception {
		
		// get settings from the config file
		String[] portSettings = ReadFile.getComPortSettings();
		
		// give the settings (params) a name
		String port = portSettings[0];
		int baudRate = Integer.parseInt(portSettings[1]);
		int dataBits = Integer.parseInt(portSettings[2]);
		int stopBits = Integer.parseInt(portSettings[3]);
		int parityBits = Integer.parseInt(portSettings[4]);
		
	    serialPort = new SerialPort(port); 
	    
	    try {
	    	
	        serialPort.openPort();//Open port
	        serialPort.setParams(baudRate, dataBits, stopBits, parityBits);//Set params
	        
	       
	       
	        // when something happens, execute code in the SerialPortReader class/eventlistener
	        serialPort.addEventListener(new SerialPortReader(serialPort));//Add SerialPortEventListener
	    }
	    catch (SerialPortException ex) {
	        System.out.println(ex);
	    }
	}
}

