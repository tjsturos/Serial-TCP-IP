package convert;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import convert.Timing;

public class TransmissionOut {
	
	public static void sendTcpOut(String code, Timing clock) {
		System.out.println("HEX Code to be sent to printer: " + code);
		
		int portNumber = 20480;
		
		  Socket myClient;
		  PrintStream output;
		    try {
		           myClient = new Socket("enp0s8", portNumber);
		           output = new PrintStream(myClient.getOutputStream());
		           output.println(code);
		           output.close();
		           myClient.close();
		    }
		    catch (IOException e) {
		        System.out.println(e);
		    }
		    
		
		// print timing after message sent
		System.out.println(clock.getTimeDifference());

	}
	
	public static void sendSerialResponse(boolean success, String codeRecieved) {
		if (success == true) {
			// do something when successfully recieved serial correctly
			System.out.println("Successfully recieved Serial code: " + codeRecieved + " and success response sent.");
		} else {
			// do something when fails to recieve message correctly
			System.out.println("Failed to interpret the serial code recieved and sent a fail response.");
		}
	}

}
