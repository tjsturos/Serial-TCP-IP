package convert;

public class SystemUtilities {
	
	public static String getRoot() {
		
		// get the operating system name
		String OS = System.getProperty("os.name").toLowerCase();
		String root;

		// decide which it is... only supporting windows and linux
		if (OS.indexOf("win") >= 0) {
		    root="C:\\";
		} else {
		    root="/tmp/";
		}
		
		return root;
	}

}
