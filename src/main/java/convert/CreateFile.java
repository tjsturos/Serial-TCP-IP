package convert;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.io.PrintWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;

import convert.SystemUtilities;
import convert.Timing;

public class CreateFile {
	
	public static void create(String[] fileDetails, Timing clock) {
		Charset utf8 = StandardCharsets.UTF_8;
		
		String fileName = fileDetails[1];
		String head = fileDetails[0];
		LocalDateTime time = LocalDateTime.now();
		
		try {
			// create the text file name
			String year = Integer.toString(time.getYear());
			String month = Integer.toString(time.getMonthValue());
			String day = Integer.toString(time.getDayOfMonth());
			String hour = Integer.toString(time.getHour());
			String minute = Integer.toString(time.getMinute());
			String second = Integer.toString(time.getSecond());
			String textFile = year + month + day + "_" + hour + minute + second + "_" + fileName + ".txt";
			
			// create the neccessary files
			String directory = fileDetails[2];
			
			
			// create rows of text to be printed in the output file (that is read by the printer controller)
			List<String> lines = Arrays.asList("FILE;".concat(directory.concat(File.separator).concat(fileName + ".xml")), "HEAD;".concat(head));
			// create text file and input correct information into it
			Path endFile= Paths.get(directory.concat(File.separator + textFile));
			System.out.println("End path is: " + endFile);
			Files.write(endFile, lines, utf8);
		
			
		} catch (Exception e) {
			System.out.println("Error---> " + e);
		}
		
		// stop time on this execution
		System.out.println(clock.getTimeDifference());
	}
	
//	public static String getSharedDirectory() {
//		
//		
//		
//		String root = SystemUtilities.getRoot();
//		
//		
//		
//		
//		try {
//			File file = new File(root + "Shared");
//			String pathName = file.getPath();
//			System.out.println("File path is: " + pathName);
//			
//			Files.createDirectories(Paths.get(pathName));
//			return pathName;
//		} catch (Exception e) {
//			System.out.println(e);
//			return null;
//		}
//
//		
//	}

}
