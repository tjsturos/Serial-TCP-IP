# Serial-TCP-IP
## Project Requirements
* To Edit: 
  - Gradle and Java IDE (AND Java 1.8+)
* To Run: 
  - Java SE (1.8+)

## Downloading Source Files
* Download ZIP from this site.
* Install git (if not already) and run `git clone https://github.com/TygerTy/Serial-TCP-IP.git` or optionally `git clone https://github.com/TygerTy/Serial-TCP-IP.git <newDirectoryName>`.

## Changing Things Up
For project changes, such as editing code i.e. source java files, then to rebuild the program you will need Gradle installed on your machine and then run the following commands:
	1. `gradle clean`
	2. `gradle fatJar`

To run the project for debugging, run this command from the project home directory: 
* `java -jar ./build/libs/SerialConverter-1.jar`
	
To do all three of the following stages at once, you can run this command from the project home directory:
	`gradle clean && gradle fatJar && clear && java -jar ./build/libs/SerialConverter-1.jar`

## Deployment
For deployment on any machine, all you need to do is run:
* `javaw -Xmx200m -jar C:\SerialConverter\SerialConverter.jar`
### Deployment on Windows
* Create a "SerialConverter" directory at the root (C:/SerialConverter).
* Save the "SerialConverter.jar" to that directory (C:/SerialConverter/SerialConverter.jar).
* Save the "config.cfg" to that directory (C:/SerialConverter/config.cfg).
* Use the included "RunSerialConverter.bat" to do the following:

#### Run a batch file at boot in Windows 98, XP, NT, 2000, Vista, and 7
* Create a shortcut to the batch file.
* Once the shortcut has been created, right-click the file and select Cut.
* Click Start, then Programs or All Programs. Find the Startup folder and right-click that folder, then select Open.
* Once the Startup folder has been opened, click Edit in the menu bar, then Paste to paste the shortcut into the startup folder. If you do not see the menu bar, press the Alt key to make the menu bar visible.

*Any shortcuts in the Startup folder will automatically run each time the user logs in to Windows.*

## Configuring the Software
The configuration file (config.cfg) has instructions included in it, but **it needs to be contained in a directory located on the root of the machine** as follows: 
Refer to the example configuration file (`exampleConfig.cfg`) for configuration options.
1. Windows: C:/SerialConverter/
2. Linux: /tmp/SerialConverter/
	
	
# Recommended installation process
	Install Java 1.8
	Save config.cfg file and SerialConverter.jar to <ROOT>/SerialConverter/
	Save RunSerialConverter.bat to <startup directory>
	
### To Do:
1. change linux directory, as the /tmp will delete everything after user logs out, requiring the user to redo the config file if they run this on a linux machine. #futureProject #onlyIfNeeded
2. Create GUI for creating the configuration file #futureProject
3. Create installer script for installing java and the SerialConverter files all in one go #futureProject
4. Create SerialReader program to monitor serial and assign values to the config file.  Perhaps as part of the GUI (3). #futureProject
	
